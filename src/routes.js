import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import SignIn from '~/pages/SignIn';
import SignUp from '~/pages/SignUp';

const SignStack = createStackNavigator();

export default function Routes() {
  return (
    <SignStack.Navigator initialRouteName="SignIn">
      <SignStack.Screen
        name="SignIn"
        component={SignIn}
        options={{ headerShown: false }}
      />
      <SignStack.Screen
        name="SignUp"
        component={SignUp}
        options={{ headerShown: false }}
      />
    </SignStack.Navigator>
  );
}
